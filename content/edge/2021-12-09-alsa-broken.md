title: "ALSA 1.2.6 breaks audio profile switching"
date: 2021-12-09
---

A recent update to the ALSA package (1.2.6) in Alpine Edge has broken audio
profile switching when phone calls are placed.

A proper fix is still being investigated. In the meantime, `alsa-*` packages
have been forked to pmaports and downgraded to 1.2.5. They show up as version
`9999-r0` when upgrading:

```
(1/6) Upgrading alsa-lib (1.2.5-r2 -> 9999-r0)
(2/5) Upgrading alsa-plugins-pulse (1.2.5-r2 -> 9999-r0)
(3/5) Upgrading alsa-ucm-conf (1.2.5.1-r1 -> 9999-r0)
(4/5) Upgrading alsa-utils (1.2.5-r2 -> 9999-r0)
(5/5) Upgrading alsa-utils-openrc (1.2.5-r2 -> 9999-r0)
```

Once a proper fix for the ALSA ucm config has been identified, the packages
will be removed and replaced with versions from Alpine's aports.

### Also see:
- [pmaports#1334](https://gitlab.com/postmarketOS/pmaports/-/issues/1334)
- [pmaports!2734](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2734)
